const navSlide = () => {
    const boton = document.querySelector(".boton-menu-responsivo");
    const menu = document.querySelector(".menu-responsivo");
  
    boton.addEventListener("click", () => {
      menu.classList.toggle("activo"),
        // animacion boton
        boton.classList.toggle("cerrar");
    });
  };
  navSlide();